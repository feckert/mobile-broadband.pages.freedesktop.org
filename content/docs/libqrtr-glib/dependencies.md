---
title: "Dependencies"
linkTitle: "Dependencies"
weight: 2
description: >
  Build and runtime dependencies of the libqrtr-glib library.
---

## Common dependencies

Before you can compile the libqrtr-glib library, you will need at least the following tools:
 * A compliant C toolchain: e.g. `glibc` or `musl libc`, `gcc` or `clang/llvm`.
 * [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/), a tool for tracking the compilation flags needed for libraries.
 * The [glib2](https://developer.gnome.org/glib/) library.
     * For libqrtr-glib >= 1.2, glib2 >= 2.56.
     * For libqrtr-glib >= 1.0, glib2 >= 2.48

In addition to the previous mandatory requirements, the project also has several optional dependencies that would be needed when enabling additional project features:
 * [gtk-doc](https://developer.gnome.org/gtk-doc-manual/stable) tools, in order to regenerate the documentation.
 * [gobject-introspection](https://gi.readthedocs.io), in order to generate the introspection support.

When building from a git checkout instead of from a source tarball, the following additional dependencies are required:
 * GNU autotools ([autoconf](https://www.gnu.org/software/autoconf/)/[automake](https://www.gnu.org/software/automake)/[libtool](https://www.gnu.org/software/libtool/)).
 * [Autoconf archive](https://www.gnu.org/software/autoconf-archive)

## Dependencies when building libqrtr-glib 1.2 or later with meson

When building with meson, the following additional dependencies are required:
 * [meson](https://mesonbuild.com/).
 * [ninja](https://ninja-build.org/).

## Dependencies when building libqrtr-glib 1.0 with GNU autotools

When building with the GNU autotools, the following additional dependencies are required:
 * [make](https://www.gnu.org/software/make/)

There are two main ways to build the library using GNU autotools: from a git repository checkout and from a source release tarball. When building from a git checkout instead of from a source tarball, the following additional dependencies are required:
 * GNU autotools ([autoconf](https://www.gnu.org/software/autoconf/)/[automake](https://www.gnu.org/software/automake)/[libtool](https://www.gnu.org/software/libtool/)).
 * [Autoconf archive](https://www.gnu.org/software/autoconf-archive).
