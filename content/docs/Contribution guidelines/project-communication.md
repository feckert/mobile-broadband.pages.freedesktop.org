---
title: "Project communication"
linkTitle: "Project communication"
weight: 5
description: >
  Different ways to communicate with the project developers
---

## Reporting bugs

Bugs should be reported through the FreeDesktop.org gitlab instance:

 * [ModemManager issues](https://gitlab.freedesktop.org/mobile-broadband/ModemManager/issues)
 * [libqmi issues](https://gitlab.freedesktop.org/mobile-broadband/libqmi/issues)
 * [libmbim issues](https://gitlab.freedesktop.org/mobile-broadband/libmbim/issues)
 * [libqrtr-glib issues](https://gitlab.freedesktop.org/mobile-broadband/libqrtr-glib/issues)

## Suggesting changes or fixes

Changes and fixes should be suggested through the FreeDesktop.org gitlab instance:
 * [ModemManager merge requests](https://gitlab.freedesktop.org/mobile-broadband/ModemManager/merge_requests)
 * [libqmi merge requests](https://gitlab.freedesktop.org/mobile-broadband/libqmi/merge_requests)
 * [libmbim merge requests](https://gitlab.freedesktop.org/mobile-broadband/libmbim/merge_requests)
 * [libqrtr-glib merge requests](https://gitlab.freedesktop.org/mobile-broadband/libqrtr-glib/merge_requests)

## Mailing lists

Formal discussion of issues, project features or any general question about the projects should be posted to the corresponding project mailing list. These mailing lists are public and archived for future reference.

Please **subscribe** to the mailing list before sending new messages, or otherwise each message sent will need to be manually approved by the list administrator.

### ModemManager mailing list

Discussions about ModemManager take place on the **modemmanager-devel (at) lists.freedesktop.org** mailing list:

 * [List info (subscribe/unsubscribe)](http://lists.freedesktop.org/mailman/listinfo/modemmanager-devel)
 * [Archives](http://lists.freedesktop.org/archives/modemmanager-devel)

### libqmi mailing list

Discussions about libqmi or libqrtr-glib take place on the **libqmi-devel (at) lists.freedesktop.org** mailing list:

 * [List info (subscribe/unsubscribe)](http://lists.freedesktop.org/mailman/listinfo/libqmi-devel)
 * [Archives](http://lists.freedesktop.org/archives/libqmi-devel)

### libmbim mailing list

Discussions about libmbim take place on the **libmbim-devel (at) lists.freedesktop.org** mailing list:

 * [List info (subscribe/unsubscribe)](http://lists.freedesktop.org/mailman/listinfo/libmbim-devel)
 * [Archives](http://lists.freedesktop.org/archives/libmbim-devel)

## Matrix and IRC

Informal discussion about the projects can be done in the project Matrix channel at **#modemmanager:matrix.org**, which is also bridged to the **#modemmanager** IRC channel in [Libera.Chat](https://libera.chat).
