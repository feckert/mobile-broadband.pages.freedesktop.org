---
title: "Building ModemManager 1.18 or earlier using GNU autotools"
linkTitle: "Autotools"
weight: 2
description: >
  How to build and install the ModemManager daemon and libraries using GNU autotools.
---

**The last stable series with support for building with the GNU autotools suite is 1.18**. All the new stable series after 1.18 will exclusively use the [meson](/docs/modemmanager/building/building-meson/) build system.

## Building from a release source tarball

The basic build and installation of the project can be done from an official release source tarball, in the following way:

```
  $ wget https://www.freedesktop.org/software/ModemManager/ModemManager-1.18.0.tar.xz
  $ tar -Jxvf ModemManager-1.18.0.tar.xz
  $ cd ModemManager-1.18.0
  $ ./configure --prefix=/usr --sysconfdir=/etc
  $ make
```

## Optional switches

Additional optional switches that may be given to the `configure` command above would be:
 * In Debian/Ubuntu systems the default location for libraries depends on the architecture of the build, so instead of the default `/usr/lib` path that would be in effect due to `--prefix=/usr`, the user should also give an explicit `--libdir` path pointing to the correct location. E.g. on a 64bit Ubuntu/Debian build, the user would use `--prefix=/usr --libdir=/usr/lib/x86_64-linux-gnu`.
 * If the MBIM protocol support is required, the additional `--with-mbim` should be given. Omitting this switch will imply auto-detecting whether the feature can be built with the already installed dependencies.
 * If the QMI protocol support is required, the additional `--with-qmi` should be given. Omitting this switch will imply auto-detecting whether the feature can be built with the already installed dependencies.
 * If the QRTR protocol support is required, the additional `--with-qrtr` should be given. Omitting this switch will imply auto-detecting whether the feature can be built with the already installed dependencies.
 * If the suspend/resume handling is required, the additional `--with-suspend-resume=systemd` should be given. Omitting this switch will imply auto-detecting whether the feature can be built with the already installed dependencies.
 * If the documentation should be rebuilt, the additional `--enable-gtk-doc` switch should be given. Omitting this switch will imply auto-detecting whether the documentation can be rebuilt with the already installed dependencies.
 * If the introspection support for the `libmm-glib` library should be included in the build, the additional `--enable-introspection` switch should be given. Omitting this switch will imply auto-detecting whether the introspection can be built with the already installed dependencies.
 * When developing changes to the project or debugging issues, it is recommended to build with debug symbols so that running the program under `gdb` produces useful backtrace information. This can be achieved by providing user compiler flags like these: `CFLAGS="-ggdb -O0"`

An example project build using all the above optional switches could be:
```
  $ ./configure                          \
      --prefix=/usr                      \
      --sysconfdir=/etc                  \
      --libdir=/usr/lib/x86_64-linux-gnu \
      --with-qmi                         \
      --with-mbim                        \
      --with-suspend-resume=systemd      \
      --enable-gtk-doc                   \
      --enable-introspection             \
      CFLAGS="-ggdb -O0"
  $ make
```

Running `./configure --help` will show all the possible switches that are supported.

## Building from a git checkout

When building from a git checkout, there is one single additional step required to build the project: running the included `autogen.sh` in order to setup the GNU autotools project and generate a `configure` script:

```
  $ git clone https://gitlab.freedesktop.org/mobile-broadband/ModemManager.git
  $ cd ModemManager
  $ NOCONFIGURE=1 ./autogen.sh
  $ ./configure --prefix=/usr --sysconfdir=/etc
  $ make
```

The same optional switches may be given to the `configure` script when building from a git checkout.

## Installing

The installation on the prefix selected during `configure` can be done with the following command:
```
  $ sudo make install
```

Please note that the command above will install the library in the system default path for libraries, possibly overwriting any previous libmm-glib library that may already exist from a package manager installed package. See [the FAQ section](/docs/faq/#can-the-projects-be-installed-in-usrlocal-instead-of-usr) for comments on how to install in `/usr/local` instead.

### Location of the DBus service files

By default the DBus service files provided by ModemManager are installed under `<prefix>/share/dbus-1/system.d/`. If the user uses the default `/usr/local` prefix instead of the suggested `/usr` prefix, the DBus service files would be installed in a path that is completely ignored by DBus. If the service files haven't changed with respect to the one provided by the package manager, this issue can probably be ignored.

### Location of the udev rules

By default the udev rules provided by ModemManager are installed under `/lib/udev/rules.d/`, out of the prefix provided by the user. If the user uses the default `/usr/local` prefix instead of the suggested `/usr` prefix, the udev rules would not change location. Changing the location of where the udev rules are installed so that the package manager provided ones are not overwritten can be done e.g. with `--with-udev-base-dir=/usr/local/lib/udev/rules.d`, but note that these rules will not be actively used by udev as they are not installed in a standard path.

### Location of the systemd init files

By default the systemd init files provided by ModemManager are installed under the path specified by the systemd library `pkg-config` file in the `systemdsystemunitdir` variable, e.g. `/lib/systemd/system/`. This path is completely independent to the prefix provided by the user. Changing the location of where the systemd init files are installed so that the package manager provided ones are not overwritten can be done e.g. with `--with-systemdsystemunitdir=/usr/local/lib/systemd/system`, but note that the init file will not be actively used by systemd as it is not installed in a standard path.

## Uninstalling

If you have manually installed the project with the steps above, it can be uninstalled in the same way:

```
  $ sudo make uninstall
```

If the manual install overwrote the package manager installed files, it is suggested to force a re-install of the corresponding packages at this point, so that the system is not left with missing files.
