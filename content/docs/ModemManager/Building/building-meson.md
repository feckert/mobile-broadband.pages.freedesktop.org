---
title: "Building ModemManager 1.18 or later with Meson"
linkTitle: "Meson"
weight: 1
description: >
  How to build and install the ModemManager daemon and libraries using the meson and ninja build systems.
---

**The first stable series with support for building with the meson suite is 1.18**. All the older stable series before 1.18 exclusively used the [GNU autotools](/docs/libqmi/building/building-autotools/) build system.

## Building from a git checkout

When using meson, the builds are always triggered from git checkouts, there is no source release tarball involved. The basic build steps would be as follows:

```
  $ git clone https://gitlab.freedesktop.org/mobile-broadband/ModemManager.git
  $ cd libqmi
  $ meson setup build --prefix=/usr --sysconfdir=/etc
  $ ninja -C build
```

## Optional switches

Additional optional switches that may be given to the `meson` command above would be:
 * In Debian/Ubuntu systems the default location for libraries depends on the architecture of the build, so instead of the default `/usr/lib` path that would be in effect due to `--prefix=/usr`, the user should also give an explicit `--libdir` path pointing to the correct location. E.g. on a 64bit Ubuntu/Debian build, the user would use `--prefix=/usr --libdir=/usr/lib/x86_64-linux-gnu`.
 * If the MBIM protocol support is required, the additional `-Dmbim=true` should be given. Omitting this switch will imply auto-detecting whether the feature can be built with the already installed dependencies.
 * If the QMI protocol support is required, the additional `-Dqmi=true` should be given. Omitting this switch will imply auto-detecting whether the feature can be built with the already installed dependencies.
 * If the QRTR protocol support is required, the additional `-Dqrtr=true` should be given. Omitting this switch will imply auto-detecting whether the feature can be built with the already installed dependencies.
 * The suspend/resume handling is enabled by default. In order to disable it, the additional `-Dsystemd_suspend_resume=false` should be given.
 * The gtk-doc documentation is disabled by default. In order to enable it, the additional `-Dgtk_doc=true` switch should be given.
 * The GObject introspection support for the `libmm-glib` library is enabled by default. In order to disable it, the additional `-Dintrospection=false` switch should be given.
 * The bash-completion support is enabled by default. In order to disable it, the additional `-Dbash_completion=false` switch should be given.
 * The default build type in meson if none explicitly specified is `debug`, which means debug symbols are included and optimization is fully disabled. The `--buildtype=release` switch can be used to remove debug symbols and to enable optimization level to the maximum.

An example project build using all the above optional switches could be:
```
  $ meson setup build                     \
      --prefix=/usr                       \
      --sysconfdir=/etc                   \
      --libdir=/usr/lib/x86_64-linux-gnu  \
      --buildtype=release                 \
      -Dqmi=true                          \
      -Dmbim=true                         \
      -Dqrtr=true                         \
      -Dsystemd_suspend_resume=false      \
      -Dgtk_doc=true                      \
      -Dintrospection=false               \
      -Dbash_completion=false
  $ ninja -C build
```

## Installing

The installation on the prefix selected during `meson setup` can be done with the following command:
```
  $ sudo ninja -C build install
```

Please note that the command above will install the library in the system default path for libraries, possibly overwriting any previous libmm-glib library that may already exist from a package manager installed package. See [the FAQ section](/docs/faq/#can-the-projects-be-installed-in-usrlocal-instead-of-usr) for comments on how to install in `/usr/local` instead.

## Uninstalling

If you have manually installed the project with the steps above, it can be uninstalled in the same way:

```
  $ sudo ninja -C build uninstall
```

If the manual install overwrote the package manager installed files, it is suggested to force a re-install of the corresponding packages at this point, so that the system is not left with missing files.
