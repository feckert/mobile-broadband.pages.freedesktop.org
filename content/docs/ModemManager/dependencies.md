---
title: "Dependencies"
linkTitle: "Dependencies"
weight: 1
description: >
  Build and runtime dependencies of the ModemManager daemon.
---

## Common dependencies

Before you can compile the ModemManager project, you will need at least the following tools:
 * A compliant C toolchain: e.g. `glibc` or `musl libc`, `gcc` or `clang/llvm`.
 * [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/), a tool for tracking the compilation flags needed for libraries.
 * The [glib2](https://developer.gnome.org/glib/) library.
     * For ModemManager >= 1.18, glib2 >= 2.56.
     * For ModemManager >= 1.14, glib2 >= 2.48
     * For ModemManager >= 1.6 and < 1.14, glib2 >= 2.36
     * For ModemManager >= 1.0 and < 1.6, glib2 >= 2.32
 * The [libgudev](https://wiki.gnome.org/Projects/libgudev) library.
     * For ModemManager >= 1.18, libgudev >= 232.
     * For ModemManager >= 1.0 and < 1.18, libgudev >= 147.
 * The [gettext](https://www.gnu.org/software/gettext) tools.
     * For ModemManager >= 1.8, gettext >= 0.19.8.
     * For ModemManager >= 1.6 and < 1.8, gettext >= 0.19.3.
     * For ModemManager >= 1.0 and < 1.6, gettext >= 0.17.

In addition to the previous mandatory requirements, the project also has several optional dependencies that would be needed when enabling additional project features:
 * **libmbim**, in order to use the MBIM protocol.
     * For ModemManager >= 1.18, libmbim >= 1.26.
     * For ModemManager >= 1.14, libmbim >= 1.24.
     * For ModemManager >= 1.8 and < 1.14, libmbim >= 1.18.
     * For ModemManager >= 1.6 and < 1.8, libmbim >= 1.14.
     * For ModemManager >= 1.0 and < 1.6, libmbim >= 1.4.
 * **libqmi**, in order to use the QMI protocol.
     * For ModemManager >= 1.18, libqmi >= 1.30.
     * For ModemManager >= 1.16, libqmi >= 1.28.
     * For ModemManager >= 1.14 and < 1.16, libqmi >= 1.26.
     * For ModemManager >= 1.12 and < 1.14, libqmi >= 1.24.
     * For ModemManager >= 1.10 and < 1.12, libqmi >= 1.22.
     * For ModemManager >= 1.8 and < 1.10, libqmi >= 1.20.
     * For ModemManager >= 1.6 and < 1.8, libqmi >= 1.16.
     * For ModemManager >= 1.2 and < 1.6, libqmi >= 1.6.
     * For ModemManager >= 1.0 and < 1.2, libqmi >= 1.4.
 * **libqrtr-glib**, in order to use Qualcomm SoCs with the QRTR bus.
     * For ModemManager >= 1.18, libqrtr-glib >= 1.0.
 * [libpolkit-gobject](https://freedesktop.org/wiki/Software/polkit) >= 0.97, in order to allow access control on the DBus methods.
 * [systemd](https://www.freedesktop.org/wiki/Software/systemd) support libraries (libsystemd >= 209 or libsystemd-login >= 183), for the optional suspend and resume support.
 * [gtk-doc](https://developer.gnome.org/gtk-doc-manual/stable) tools, in order to regenerate the documentation.
 * [gobject-introspection](https://gi.readthedocs.io), in order to generate the introspection support.

## Dependencies when building ModemManager 1.18 or later with meson

When building with meson, the following additional dependencies are required:
 * [meson](https://mesonbuild.com/).
 * [ninja](https://ninja-build.org/).

The following optional dependencies are available when building with meson:
 * [bash-completion](https://github.com/scop/bash-completion), in order to add completion support for the command line tools.

## Dependencies when building ModemManager 1.18 or earlier with GNU autotools

When building with the GNU autotools, the following additional dependencies are required:
 * [make](https://www.gnu.org/software/make/)

There are two main ways to build the library using GNU autotools: from a git repository checkout and from a source release tarball. When building from a git checkout instead of from a source tarball, the following additional dependencies are required:
 * GNU autotools ([autoconf](https://www.gnu.org/software/autoconf/)/[automake](https://www.gnu.org/software/automake)/[libtool](https://www.gnu.org/software/libtool/)).
 * [Autoconf archive](https://www.gnu.org/software/autoconf-archive), in the 1.16 series exclusively.
