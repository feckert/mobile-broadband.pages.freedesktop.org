---
title: "The MBIM protocol and the Microsoft extensions"
linkTitle: "The MBIM protocol and the Microsoft extensions"
weight: 4
description: >
  A general overview of the Mobile Broadband Interface Modem (MBIM) protocol and the service extensions implemented by Microsoft to support features of new technologies like 5G.
---

## The MBIM standard

The **Mobile Interface Broadband Model** (MBIM) is a standard created by the [USB Implementers Forum](https://www.usb.org) for high-speed mobile broadband modem devices. This standard defines a new USB networking subclass for communication between the host system and the modem device, with two separate features:
 * A MBIM **USB device model** that provides multiple IP connections over a single USB interface without the need for 802.3 frames (as was the case with NCM and other models for Ethernet over USB). Data transferred over this type of network interfaces are IP-only packets, without any additional link level header.
 * A MBIM **control protocol** for communicating with modem devices. This protocol is exclusively used in the interface **between the host and the modem**. It has no direct mapping with the networking protocols used by the modem in the radio interface.

![USB standards](/images/mbim-protocol-usb-standards.png)

The “Universal Serial Bus Communications Class Subclass Specification for Mobile Broadband Interface Model - Version 1.0, Errata 1” (**MBIM 1.0** from now on) was released on May 1st, 2013 and is the last official version of the original protocol specification. The definition of this protocol was a joint effort between different companies like Ericsson, Microsoft, Qualcomm, Nokia, and others.

The standard covered both 3GPP (GSM/UMTS/LTE) and 3GPP2 (CDMA/EVDO) devices, but effectively only the former ones have been integrated with MBIM support. There are currently no 3GPP2 devices with MBIM support available, as 3GPP2 technologies have been gradually phased out.

The standard was successful primarily because Microsoft used it as a reference implementation for WWAN management in their Windows operating system. Devices that support this specification would not need additional vendor-specific drivers to be properly integrated into the system's network stack.

To take full advantage of 5G devices, which have data rates that are higher than what USB can support, modems now support connections to the PCI bus in addition to (or instead of) USB. The original USB device model is not used in PCI mode, but the MBIM control protocol can still be used to communicate with the device.

## The MBIM control protocol

The protocol defines the communication between the host and the modem by providing:
 * Different **message types** for different purposes.
 * Different **services** grouping operations that are related to each other.
 * Different **request/responses** and **indications** in each service.

The protocol assumes that there is a single control point in the host. This control point is responsible for managing the **MBIM session** by opening it explicitly when communication begins and closing it explicitly when communication is no longer expected. The host is not able to perform any operation on the device if there is no open MBIM session.

### MBIM messages

#### Message types

The protocol defines different **binary** message types with different formats.

Some of them are used to establish the channel of communication with the modem:
 * **Open Message** (Host->Modem): Initialization request.
 * **Open Done Message** (Host<-Modem): Initialization response.
 * **Close Message** (Host->Modem): Close request.
 * **Close Done Message** (Host<-Modem): Close response.

Some of the messages are used to report errors in the protocol; which may be sent either from the host or from the modem:
 * **Host Error Message** (Host->Modem): Host-reported error.
 * **Modem Error Message** (Host<-Modem): Modem-reported error.

And finally, some messages provide access to the different operations defined in each service. All these message types have theoretical full message length of up to 2^32 bytes, and are split into separate **transactions** as appropriate.
 * **Command Message** (Host->Modem): Request of a given command: either SET or GET.
 * **Command Done Message** (Host<-Modem): Response to a given command.
 * **Indication Message** (Host<-Modem): Unsolicited messages sent by the modem. Indication messages may be enabled or disabled by the host explicitly.

![MBIM session](/images/mbim-protocol-mbim-session.png)

#### Message format

The format of the MBIM messages defined in the MBIM 1.0 protocol specification is fixed and strict, and has certain interesting properties that make it easier to process:
 * The messages define a fixed amount of fields, which are always included.
 * The messages are built with a fixed-sized block first containing the fixed-sized field contents (e.g. integers, uuid), followed by a variable-sized block containing the data of fields that don’t have a fixed size (e.g. strings, arrays).
 * Integers are encoded either in 32bits (even for smaller types like booleans or 16-bit numbers), or in 64 bits, always in little endian.
 * Variable-sized fields are defined first in the fixed-sized block via an “OL pair”. This is a pair of 32bit integers defining the byte offset inside the variable-sized block where the data of the field resides, and the length of the data in bytes.
 * All data contents of fields in the variable-sized block are padded to 32bits.
 * Arrays of elements (either integers, strings or structs) are supported. The array field is defined as an “OL pair”. The data of the array that is defined in the variable-sized block of the message may itself be split into its own fixed-sized and variable-sized blocks.
 * All strings are encoded in UTF-16LE and limited to the code points inside the Basic Multilingual Plane, padded to 32bits when necessary.
 * All fields aligned at 32bit boundaries!

The following image shows an example of a MBIM 1.0 **Register State response** message.

![MBIM Register State response](/images/mbim-protocol-register-state.png)

### MBIM 1.0 services

#### The Basic Connect service

The Basic Connect service is the most useful service among the generic ones, as it provides all the basic capabilities the host requires to control the modem device. This service provides operations to query SIM status, unlock PIN, query and request network registration, manage data connectivity and many more.

The full list of operations defined in the MBIM protocol specification for this service is as follows (mandatory ones given in **bold**):

 * **MBIM_CID_DEVICE_CAPS: Device capabilities.** This message allows querying for the capabilities of the modem, e.g. to know whether it is a 3GPP (GSM/UMTS/LTE) or a 3GPP2 (CDMA/EVDO) device.
 * **MBIM_CID_SUBSCRIBER_READY_STATUS: Subscriber ready status.** This message allows querying for the details and state of the SIM card.
 * **MBIM_CID_RADIO_STATE: Radio state.** This message allows controlling the power state of the radio interface.
 * **MBIM_CID_PIN: PIN.** This message allows unlocking/enabling/disabling the PIN lock (e.g. SIM-PIN or SIM-PUK).
 * MBIM_CID_PIN_LIST: PIN list. This message returns a list of all lock types supported by the modem and the status of each of them.
 * **MBIM_CID_HOME_PROVIDER: Home provider.** This message provides information about the home provider as per the current SIM card in use.
 * MBIM_CID_PREFERRED_PROVIDERS: Preferred providers. This command allows querying the list of preferred providers, e.g. to know which operator would be used when roaming in a different network.
 * MBIM_CID_VISIBLE_PROVIDERS: Visible providers. This message allows requesting a network scan to look for service providers nearby.
 * **MBIM_CID_REGISTER_STATE: Register state.** This command allows management of the network registration, e.g. to know the current registration state or to manually lock into a specific operator.
 * **MBIM_CID_PACKET_SERVICE: Packet service.** This command allows management of the packet service domain state.
 * **MBIM_CID_SIGNAL_STATE: Signal state.** This command allows querying and getting notified of the signal state information.
 * **MBIM_CID_CONNECT: Connect.** This command allows activating a new packet data connection.
 * MBIM_CID_PROVISIONED_CONTEXTS: Provisioned contexts. This command allows managing the list of profiles stored in the modem. Profiles are settings of different contexts/bearers that may be used to activate data connections.
 * MBIM_CID_SERVICE_ACTIVATION: Service activation (e.g. in CDMA networks).
 * **MBIM_CID_IP_CONFIGURATION: IP configuration.** This command allows querying the IP configuration the modem has agreed with the network once the connection has been established.
 * **MBIM_CID_DEVICE_SERVICES: Device services.** This command allows querying which MBIM services are supported by the device.
 * MBIM_CID_DEVICE_SERVICE_SUBSCRIBE_LIST: Device service subscribe list. This command allows managing the subscription for the different indications the modem may emit.
 * MBIM_CID_PACKET_STATISTICS: Packet statistics. This command allows querying the packet statistics of the ongoing data connections.
 * MBIM_CID_NETWORK_IDLE_HINT: Network idle hint. This command may be used either by host or modem to notify the peer that there is no network traffic expected, e.g. if the system is entering some idle state.
 * MBIM_CID_EMERGENCY_MODE: Emergency mode, allows reporting whether emergency services are supported by the modem.
 * MBIM_CID_IP_PACKET_FILTERS: IP packet filters. This command allows the host to manage a set of filters (applicable to the IP stream exclusively) to let the mode wake up the host upon receiving certain packets.
 * MBIM_CID_MULTICARRIER_PROVIDERS: Multicarrier providers. This message allows managing devices that may initially be bound to a certain carrier and can be configured to work with a different one. E.g. in CDMA networks where SIM cards are not used and the device itself is the one bound to the carrier through service activation.

#### The SMS service

The SMS service provides operations to manage [Short Messaging Service](https://en.wikipedia.org/wiki/SMS) messages in the device. It allows reading, sending and deleting messages, and configuring certain aspects of the SMS management logic, like the SMSC address.

The full list of operations defined in the MBIM protocol specification for this service is as follows:
 * MBIM_CID_SMS_CONFIGURATION: SMS configuration.
 * MBIM_CID_SMS_READ: Read.
 * MBIM_CID_SMS_SEND: Send.
 * MBIM_CID_SMS_DELETE: Delete.
 * MBIM_CID_SMS_MESSAGE_STORE_STATUS: Store message status.

One important missing feature in this service is that it completely hides the existence of multiple SMS storages (e.g. SIM vs device). The service also lacks the support to store SMS messages before sending them.

#### The USSD service

The USSD service allows performing standard [Unstructured Supplementary Service Data](https://en.wikipedia.org/wiki/Unstructured_Supplementary_Service_Data) operations to exchange requests and responses with the serving network (e.g. to request top-up of the prepaid card balance).

 * MBIM_CID_USSD: USSD operation.

#### The Phonebook service

The Phonebook service allows managing user contact information. The MBIM standard is not designed to support voice call management, so the phonebook management service is exclusively thought to be used along with the SMS operations (e.g. to associate phone numbers in the short messages with contact entries in the phonebook).

 * MBIM_CID_PHONEBOOK_CONFIGURATION: Configuration.
 * MBIM_CID_PHONEBOOK_READ: Read.
 * MBIM_CID_PHONEBOOK_DELETE: Delete.
 * MBIM_CID_PHONEBOOK_WRITE: Write.

As with the SMS case, the service lacks the support to expose the existence of multiple phonebook storages (e.g. SIM vs device).

#### The STK service

The STK service allows performing [SIM Application Tookit](https://en.wikipedia.org/wiki/SIM_Application_Toolkit) operations as defined in the GSM standard. This service allows SIM cards to run simple applications for value-added services (e.g. browsing the Internet or reading email). STK is equivalent to USAT (USIM Application Toolkit) in USIMs or CAT (Card Application Toolkit) in UICC cards.

 * MBIM_CID_STK_PAC: PAC.
 * MBIM_CID_STK_TERMINAL_RESPONSE: Terminal response.
 * MBIM_CID_STK_ENVELOPE: Envelope.

STK applications were once very common on early GSM devices that used WAP to access the internet, but they are no longer used as much.

#### The AUTH service

The AUTH service allows sending authentication challenges to the device using either the [EAP-AKA](https://en.wikipedia.org/wiki/Extensible_Authentication_Protocol#EAP_Authentication_and_Key_Agreement_(EAP-AKA)), [EAP-AKA’](https://en.wikipedia.org/wiki/Extensible_Authentication_Protocol#EAP_Authentication_and_Key_Agreement_prime_(EAP-AKA')) or [EAP-SIM](https://en.wikipedia.org/wiki/Extensible_Authentication_Protocol#EAP_Subscriber_Identity_Module_(EAP-SIM)) authentication procedures.

 * MBIM_CID_AUTH_AKA: EAP Authentication and Key Agreement (used in 3GPP UMTS networks).
 * MBIM_CID_AUTH_AKAP: EAP Authentication and Key Agreement prime (used in non-3GPP networks like WiMAX, CDMA or EV-DO).
 * MBIM_CID_AUTH_SIM: EAP Subscriber Identity Module (used in 3GPP GSM networks).

These user operations are not necessary in standard 3GPP modems, as the modem itself performs mutual authentication between the SIM card and the network through the radio interface connection. However, they are useful in devices that are trying to register with a 3GPP network through non-3GPP access (e.g. WiFi).

#### The DSS service

Device Service Streams (DSS) allow transferring large amounts of non-IP traffic data from modem to host and vice versa via USB bulk-in and bulk-out pipes. This enables different types of services to provide a channel with high bandwidth without the limitation inherent in the MBIM control port.

 * MBIM_CID_DSS_CONNECT: Connect.

E.g. Microsoft plans to use this DSS service for [modem log collection](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-modem-logging-with-dss#mbim_cid_modem_logging_config).

E.g. [Huawei patented](https://patents.google.com/patent/CN104113823B/en) GNSS data over DSS in MBIM back in 2013, which is probably why we’re still stuck with serial ports for that same purpose.

## The Microsoft Extensions

A team of engineers from different companies, each of them very likely with different needs and approaches, wrote the original MBIM 1.0 specification. Unfortunately, the scope of the reference was limited to the technologies used at the time of writing, and the protocol itself (e.g. strict message formats) didn’t leave room for easy updates without breaking the API. To solve the problem, major users of the protocol ended up doing their own updates.

Microsoft has been the major lead of MBIM protocol updates, as it integrates seamlessly with their Windows operating system. The updates have happened in two different ways trying not to break API of already existing messages:
 * New service additions with new messages and enumeration types for either different features or new functionality.
 * Update of already existing services via a method of negotiating the version of the messages to use between host and device.

The following sections define MBIM messages originally defined by Microsoft in *italics* (e.g. *MBIM_CID_MS_PROVISIONED_CONTEXT_V2*), while keeping the name of the messages without italics for commands that were originally defined in the MBIM 1.0 specification, even if later updated by Microsoft (e.g. MBIM_CID_CONNECT).

### Microsoft defined services

#### The Microsoft Basic Connect Extensions service

This service implements support for several new operations, including [multi-executor/multi-SIM support](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-multi-sim-operations) (e.g. if the modem has separate controllable radio stacks and/or SIM cards), [EPS initial bearer configuration/status querying](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-lte-attach-operations) (i.e. attach APN) and many more. All these features are considered essential for the correct configuration and management of the device.

Given that all these messages are implemented in a completely separate service, there is no collision with other equivalent messages defined in the MBIM 1.0 specification. In this case, the protocol user should choose which message to use: the generic one from MBIM 1.0 or the specific one from the extensions service.

 * *MBIM_CID_MS_PROVISIONED_CONTEXT_V2*: [Provisioned contexts](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-provisioned-context-operations). This is a message equivalent to the generic MBIM_CID_PROVISIONED_CONTEXTS but with additions to the profile contents, like roaming allowance, access type or profile source information. One extremely important change in this update is that the unique identifier of the different provisioned contexts is no longer a numeric context id, but instead the **APN type**. In the Microsoft extensions different contexts are configured for different purposes; so one context may be for Internet traffic, while another one is for SUPL server access. This allows modem-initiated applications to unequivocally know the expected PDU session settings to use depending on the application type, without intervention from the host. The APN type does not serve any purpose in the communication of the modem with the network, it’s purpose is bound to the host and the modem only.
 * *MBIM_CID_MS_NETWORK_BLACKLIST*: [Network denylist](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-network-blacklist-operations).
 * *MBIM_CID_MS_LTE_ATTACH_CONFIG*: [LTE attach configuration](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-lte-attach-operations#mbim_cid_ms_lte_attach_config).
 * *MBIM_CID_MS_LTE_ATTACH_STATUS*: [LTE attach status information](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-lte-attach-operations#mbim_cid_ms_lte_attach_status).
 * *MBIM_CID_MS_SYS_CAPS*: [System capabilities](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-multi-sim-operations#mbim_cid_ms_sys_caps).
 * *MBIM_CID_MS_DEVICE_CAPS_V2*: [Device capabilities](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-multi-sim-operations#mbim_cid_ms_device_caps_v2). This is a message equivalent to the generic MBIM_CID_DEVICE_CAPS but with additional information for the multi-executor support.
 * *MBIM_CID_MS_DEVICE_SLOT_MAPPINGS*: [Device slot mappings](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-multi-sim-operations#mbim_cid_ms_device_slot_mappings).
 * *MBIM_CID_MS_SLOT_INFO_STATUS*: [Slot info status](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-multi-sim-operations#mbim_cid_ms_slot_info_status).
 * *MBIM_CID_PCO*: [Protocol configuration operations (PCO)](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-protocol-configuration-options-pco-operations).
 * *MBIM_CID_MS_DEVICE_RESET*: [Device reset](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-modem-reset-operations).
 * *MBIM_CID_BASE_STATIONS_INFO*: [Base stations info](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-base-stations-information-query-support#mbim_cid_base_stations_info).
 * *MBIM_CID_LOCATION_INFO_STATUS*: [Location info status](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-base-stations-information-query-support#mbim_cid_location_info_status).
 * *MBIM_CID_VERSION*: [Protocol version query and report](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mbimex-2.0-5g-nsa-support#mbim_cid_version).
 * *MBIM_CID_MODEM_LOGGING_CONFIG*: [Modem logging configuration](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-modem-logging-with-dss#mbim_cid_modem_logging_config). Still not fully defined (e.g. missing command id).

### Other feature-specific services

In addition to the new basic operations implemented in the Basic Connect Extensions service, Microsoft also defined additional services for different new features, including for example:
 * The Microsoft [Firmware ID service](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mobile-broadband-device-firmware-update).
 * The Microsoft [Host Shutdown service](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/host-shutdown-device-service).
 * The Microsoft [SAR service](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-sar-platform-support).
 * The Microsoft [UICC low level access service](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-low-level-uicc-access).
 * The Microsoft [Voice extensions service](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mb-nitz-support) (misleading name, the only supported command is to query network identity and time zone information).

This list is not exhaustive, additional services or messages may exist.

### Versioned updates on already existing services

One thing that Microsoft probably realized was that the strictly fixed format of MBIM messages does not leave much room for incremental updates of their contents. As seen in the MBIM_CID_MS_DEVICE_CAPS_V2, the contents of the original MBIM_CID_DEVICE_CAPS message were updated just to add one single integer value at the end of the message.

The solution to avoid needing completely new messages when existing ones need to be updated was providing a [versioned approach](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mbimex-2.0-5g-nsa-support#versioning-scheme) for all existing messages (both the ones defined in the MBIM 1.0 standard and the Microsoft extensions).

When the host system boots, the process managing the MBIM operations will open the MBIM session, perform generic device service and command support identification, and then send a **MBIM_CID_VERSION** command to the modem, specifying which are the MBIM version (**MBIM release number**) and MBIMEx version (the **MBIM Extensions release number**) the host supports. The modem may support newer or older releases, so knowing which is the one supported by the host, it decides the final agreed version and reports it back to the host. In order for this logic to work properly, the software running in the host must support new MBIMEx releases incrementally, without losing the support of older ones.

 * The MBIM release number is 1.0 by default, and is not expected to change any time soon as there are no new official MBIM releases planned by the USB-IF.
 * The MBIMEx release number goes **from 2.0 to 4.0** (as of this writing). No modem will report MBIMEx v1.0 supported because the specific command used to negotiate the versioning is implemented only in devices supporting at least MBIMEx v2.0 (i.e. if the command is not supported, the host can assume it must not use any versioned MBIM command).

The following flow shows an example agreement between the host supporting MBIMEx v3.0 and the modem supporting MBIMEx v2.0. The version agreed will always be the lowest common denominator of both version numbers, i.e. MBIMEx v2.0 in this case.

![MBIM version agreement](/images/mbim-protocol-version-agreement.png)

#### MBIMEx v2.0: NSA support

The [MBIMEx v2.0 update](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mbimex-2.0-5g-nsa-support) introduces support for modems capable of **5G NSA** connectivity. It provides updates for already existing messages, which will be available once the negotiated MBIMEx version support is at least v2.0.

 * MBIM_CID_REGISTER_STATE: [Register state](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mbimex-2.0-5g-nsa-support#mbim_cid_register_state). This update of the original MBIM 1.0 command introduces an additional field to report the preferred data classes.
 * MBIM_CID_PACKET_SERVICE: [Packet service state](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mbimex-2.0-5g-nsa-support#mbim_cid_packet_service). This update of the original MBIM 1.0 command introduces a new frequency range field that allows reporting whether the 5G service is sub-6GHz or mmWave.
 * MBIM_CID_SIGNAL_STATE: [Signal state](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mbimex-2.0-5g-nsa-support#mbim_cid_signal_state). This update of the original MBIM 1.0 command introduces reporting RSRP and SNR per available technology.
 * *MBIM_CID_MS_DEVICE_CAPS_V2*: [Device capabilities](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mbimex-2.0-5g-nsa-support#mbim_cid_ms_device_caps_v2). This is not a real new message defined, it only extends the list of possible data class values to include 5G.

#### MBIMEx v3.0: 5G SA phase 1 support

The [MBIMEx v3.0 update](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mbimex-3.0-5g-sa-phase-1-support) introduces support for basic 5G SA specific features.

The following already existing messages are updated:

 * *MBIM_CID_MS_DEVICE_CAPS_V2*: Device capabilities. This update deprecates certain data class values that were introduced in MBIMEx v2.0 and adds new ones, along with a new data subclass field to report 5G-specific configurations (e.g. which exact type of 5G NSA is being used, like EN-DC or NE-DC). The command also provides information about supported LTE and 5GNR bands and many other things.
 * MBIM_CID_SUBSCRIBER_READY_STATUS: Subscriber ready status. This update of the original MBIM 1.0 command includes information about the SIM type (e.g. whether it’s a physical SIM or an eSIM) and whether the SIM is removable or not.
 * MBIM_CID_PACKET_SERVICE: Packet service state. This update of the original MBIM 1.0 command uses the new data subclass type, and introduces a new Tracking Area Identity (TAI) field.
 * MBIM_CID_CONNECT: Connect. This update of the original MBIM 1.0 command increases the amount of fields that may be provided in a connection attempt (e.g. access type preference, APN type). It also changes how certain other fields like strings are formatted.
 * MBIM_CID_IP_PACKET_FILTERS: IP packet filters. This update of the original MBIM 1.0 command introduces a new Filter ID field.
 * *MBIM_CID_MS_BASE_STATIONS_INFO*: Base stations info. This update adds support for reporting 5G NR cell measurements.
 * *MBIM_CID_MS_LTE_ATTACH_STATUS*: LTE attach status information). This update includes support for reporting the specific error emitted by the network upon an attach failure.

The following new messages are introduced:
 * *MBIM_CID_MS_MODEM_CONFIG*: Modem configuration status. This new message allows the modem to report the status of modem configuration loading performed once a new SIM is activated in the modem.
 * *MBIM_CID_MS_REGISTRATION_PARAMS*: Registration parameters. This new message is somewhat equivalent to the attach configuration support available for LTE devices, but for the 5G registration procedure. It provides settings that should be used during registration to the network, and allows specifying e.g. which 5G network slices the modem should connect to.
 * *MBIM_CID_MS_WAKE_REASON*: Wake reason query and report. This new message allows the host to query the modem for the reason why it was woken up.

#### MBIMEx v4.0: 5G SA phase 2 support

The [MBIMEx v4.0 update](https://learn.microsoft.com/en-us/windows-hardware/drivers/network/mbimex-4.0-5g-sa-phase-2-support) introduces support for advanced 5G SA specific features. This phase aims to support end-to-end User Equipment Route Selection (URSP) handling and multiple concurrent eMBB network slices. All valid slice types (SST) are supported at the MBIM protocol level. Functional requirements for non-eMBB slices are not implied and are subject to additional host and device-level support and features.

The following already existing messages are updated:
 * *MBIM_CID_MS_DEVICE_CAPS_V2*: Device capabilities. The format of the message is not modified, but the list of supported capabilities now also reports whether the device is able to use URSP rules while not registered in the 5G core network (e.g. if registered only in the 4G core network).
 * *MBIM_CID_MS_REGISTRATION_PARAMS*: Registration parameters. The message supports a new optional field including the Operating System ID (OSID). The device may use the host’s OSID in the communication with the network.
 * MBIM_CID_SUBSCRIBER_READY_STATUS: Subscriber ready status. This update of the original MBIM 1.0 command allows specifying the exact SIM slot ID where the action should be performed. This allows querying the information and state of the SIM card that is reachable in the slot that is considered inactive for network registration purposes.
 * MBIM_CID_PACKET_SERVICE: Packet service. This update of the original MBIM 1.0 command adds support for a new optional field including the 3GPP release number currently implemented by the network.
 * MBIM_CID_CONNECT: Connect. This update of the original MBIM 1.0 command includes a new activation state field, which allows the host to specify how the modem should use the URSP rules to activate the connection or match an already established one. It also includes support for defining the 5G slice id to use in the PDU session setup.
 * MBIM_CID_RADIO_STATE: Radio state. The format of the original MBIM 1.0 command is not modified, but the description of its fields is updated. In particular, the specification now defines that the host-controlled power state of the modem must initially be always off, and the modem should never change that automatically by itself. This means that the modems would always boot with radio communications disabled (e.g. airplane mode) and only enable them once the host requests to do so.
 * *MBIM_CID_MS_PROVISIONED_CONTEXT_V2*: Provisioned contexts v2. This message is updated to allow defining 5G slice ids to profiles stored in the modems.

The following new messages are introduced:
 * *MBIM_CID_MS_NETWORK_PARAMS*: Network parameters. This new message allows the host to be notified of network-initiated configuration changes (e.g. allowed or rejected slices), for example when getting into a different tracking area.
 * *MBIM_CID_MS_UE_POLICY*: UE policy. This new message allows the host to be notified of network-initiated UE policy changes (URSP rules as per 3GPP TS 24.526).

The MBIMEx v4.0 update also includes updates to other feature-specific services, such as the UICC low-level access service.


### Message format in Microsoft Extensions

The format of the messages in the new Microsoft extensions is mostly similar to the one in the MBIM 1.0 specification, but they include certain features that make them more dynamic and extensible. However, not all the changes to the format have been made with care. In some instances, the changes introduced are unnecessary and add more complexity than anything else.

 * MBIMEx v3.0 introduces Type-Length-Value (TLV) fields for both basic (e.g. strings) and complex data structures. Unlike other protocols like Qualcomm’s QMI, the values stored in the TLVs defined by Microsoft do not follow a common structure. Each TLV value content has its own specific format.
 * TLV fields may be mandatory (i.e. explicitly defined in the contents of a given message) or optional (added in a new generic “Unnamed IEs” field at the end of the message.
 * Certain messages will use LO (length+offset) pairs instead of OL (offset+length) pairs to define position and size of the data in the variable-sized buffer. This totally looks like an oversight in the protocol definition.
 * Certain messages in the MBIM extensions define fixed-sized contents (e.g. structs with a fixed amount of fields with a known size) also in the variable-sized buffer, by providing an OL pair. This actually has a useful side effect, which is that the fields can be treated as optional (for example, if the OL pair specifies a 0-byte length).
 * Certain messages will include strings encoded in UTF-8 instead of UTF-16LE.
 * Integer types smaller than 32bits are introduced, e.g. 16bits.
 * Certain messages introduce [unreasonably complex types](https://github.com/MicrosoftDocs/windows-driver-docs/issues/3247), e.g. an array of bytes where the length field represents the size of the original array, but which only up to 255 items are included in the array itself (i.e. size may be reported as 1024 but only 255 bytes given).
 * The contents of the messages are not always guaranteed to be strictly aligned at 32bit boundaries any more.

## Other services from manufacturers and operators

In addition to the extensions defined by Microsoft, other companies have also implemented their own MBIM services for different purposes. To name a few:
 * AT&T defines the **ATDS service** that supports methods to query detailed signal quality information (e.g. RSRP, RSRQ) in LTE capable devices. This logic is now superseded by the Microsoft extensions, which are also capable of reporting 5GNR signal quality information.
 * Intel defines a **Firmware Update service** with commands to request rebooting the module into firmware download mode.
 * Qualcomm defines a **QMI service** that allows running any kind of QMI operation over MBIM transparently. All Qualcomm-based MBIM modems support this.
 * Quectel defines a **QDU service** that supports performing the whole device firmware upgrade operation (including file download to the module) via MBIM requests and responses.
