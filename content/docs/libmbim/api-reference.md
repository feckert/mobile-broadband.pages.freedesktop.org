---
title: "API reference"
linkTitle: "API reference"
weight: 1
description: >
  Reference manual for the libmbim-glib library.
---

The `libmbim-glib` API reference provides a detailed list of operations that may be performed with MBIM devices.

Most of the documentation pages are automatically generated from the database of messages that the project maintains, and therefore it won't give information about the purpose of the operations, or the exact format of the retrieved fields. The documentation is anyway extremely helpful when writing software using the library, as it provides a quick way to browse the interfaces and the expected fields in each message.

In order to know the exact purpose of each message or the format of the retrieved fields, please refer to the generic MBIM services references from the [USB-IF](https://www.usb.org/documents) or to the vendor-specific service references. Most of these documents are publicly available and don't require any NDA.

## Online references

The most up to date API reference of the `libmbim-glib` library is kept in the following location:

 * [Latest](https://www.freedesktop.org/software/libmbim/libmbim-glib/latest/)

The full list of API references published is kept for future reference:

 * [1.28.0](https://www.freedesktop.org/software/libmbim/libmbim-glib/1.28.0/)
 * [1.26.0](https://www.freedesktop.org/software/libmbim/libmbim-glib/1.26.0/)
 * [1.24.0](https://www.freedesktop.org/software/libmbim/libmbim-glib/1.24.0/)
 * [1.22.0](https://www.freedesktop.org/software/libmbim/libmbim-glib/1.22.0/)
 * [1.20.0](https://www.freedesktop.org/software/libmbim/libmbim-glib/1.20.0/)
 * [1.18.0](https://www.freedesktop.org/software/libmbim/libmbim-glib/1.18.0/)
 * [1.16.0](https://www.freedesktop.org/software/libmbim/libmbim-glib/1.16.0/)
 * [1.14.0](https://www.freedesktop.org/software/libmbim/libmbim-glib/1.14.0/)
 * [1.12.0](https://www.freedesktop.org/software/libmbim/libmbim-glib/1.12.0/)
 * [1.10.2](https://www.freedesktop.org/software/libmbim/libmbim-glib/1.10.2/), [1.10.0](https://www.freedesktop.org/software/libmbim/libmbim-glib/1.10.0/)
 * [1.8.0](https://www.freedesktop.org/software/libmbim/libmbim-glib/1.8.0/)
 * [1.6.0](https://www.freedesktop.org/software/libmbim/libmbim-glib/1.6.0/)
 * [1.4.0](https://www.freedesktop.org/software/libmbim/libmbim-glib/1.4.0/)
 * [1.2.0](https://www.freedesktop.org/software/libmbim/libmbim-glib/1.2.0/)
 * [1.0.0](https://www.freedesktop.org/software/libmbim/libmbim-glib/1.0.0/)

There is no API reference published for stable release updates that didn't have any API change.

## Local reference

The API reference is usually installed along with the `libmbim-glib` library (sometimes as a separate distribution package) under `/usr/share/gtk-doc/html/libmbim-glib/` and can be browsed locally via the [Devhelp](https://wiki.gnome.org/Apps/Devhelp) tool. The version of the installed reference will be the one applying to the `libmbim-glib` library installed in the system.
